<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Club State Field -->
<div class="form-group col-sm-6">
    {!! Form::label('club_state', 'Club State:') !!}
    {!! Form::text('club_state', null, ['class' => 'form-control']) !!}
</div>

<!-- Logo Uri Field -->
<div class="form-group col-sm-6">
    {!! Form::label('logo_uri', 'Logo Uri:') !!}
    <div>
    	<div class="col-sm-6 float-left">
		    {!! Form::file('logo_uri', null, ['class' => 'form-control']) !!}
		</div>
		@if(!empty($team))
	    <div class="col-sm-6 float-right">
	    	<img src="{!! asset('/storage/team_logos/'.$team->logo_uri) !!}" height="100" />
	    </div>
	    @endif
	</div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('teams.index') !!}" class="btn btn-default">Cancel</a>
</div>
