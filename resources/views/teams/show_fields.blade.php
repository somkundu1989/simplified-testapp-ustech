<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $team->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $team->name !!}</p>
</div>

<!-- Logo Uri Field -->
<div class="form-group">
    {!! Form::label('logo_uri', 'Logo Uri:') !!}
    <p><img src="{!! asset('/storage/team_logos/'.$team->logo_uri) !!}" height="100" /></p>
</div>

<!-- Club State Field -->
<div class="form-group">
    {!! Form::label('club_state', 'Club State:') !!}
    <p>{!! $team->club_state !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $team->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $team->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $team->deleted_at !!}</p>
</div>

