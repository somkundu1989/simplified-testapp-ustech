<li class="{{ Request::is('teams*') ? 'active' : '' }}">
    <a href="{!! route('teams.index') !!}"><i class="fa fa-edit"></i><span>Teams</span></a>
</li>

<li class="{{ Request::is('players*') ? 'active' : '' }}">
    <a href="{!! route('players.index') !!}"><i class="fa fa-edit"></i><span>Players</span></a>
</li>

<li class="{{ Request::is('matches*') ? 'active' : '' }}">
    <a href="{!! route('matches.index') !!}"><i class="fa fa-edit"></i><span>Matches</span></a>
</li>

<li class="{{ Request::is('points*') ? 'active' : '' }}">
    <a href="{!! route('points.index') !!}"><i class="fa fa-edit"></i><span>Points</span></a>
</li>
