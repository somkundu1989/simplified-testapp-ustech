<!-- Team Id Field -->
@php

#dd($teams);

@endphp

<div class="form-group col-sm-6">
    {!! Form::label('team_id', 'Team:') !!}
    {{ Form::select('team_id', $teams, null, ['class' => 'form-control']) }}
</div>

<!-- Jersey Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jersey_number', 'Jersey Number:') !!}
    {!! Form::number('jersey_number', null, ['class' => 'form-control']) !!}
</div>

<!-- First Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country', 'Country:') !!}
    {!! Form::text('country', null, ['class' => 'form-control']) !!}
</div>

<!-- Details Field -->
<div class="form-group col-sm-6">
    {!! Form::label('details', 'Play History / Details:') !!}
    {!! Form::textarea('details', null, ['class' => 'form-control', 'rows'=>2]) !!}
</div>

<!-- Image Uri Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image_uri', 'Image Uri:') !!}
    <div>
        <div class="col-sm-6 float-left">
            {!! Form::file('image_uri', null, ['class' => 'form-control']) !!}
        </div>
        @if(!empty($player))
        <div class="col-sm-6 float-right">
            <img src="{!! asset('/storage/player_images/'.$player->image_uri) !!}" height="100" />
        </div>
        @endif
    </div>
   
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('players.index') !!}" class="btn btn-default">Cancel</a>
</div>
