<div class="table-responsive">
    <table class="table" id="players-table">
        <thead>
            <tr>
                <th>Team</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Image Uri</th>
                <th>Jersey Number</th>
                <th>Country</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($players as $player)
            <tr>
                <td>{!! $player->team->name !!}</td>
                <td>{!! $player->first_name !!}</td>
                <td>{!! $player->last_name !!}</td>
                <td><img src="{!! asset('/storage/player_images/'.$player->image_uri) !!}" height="30" /></td>
                <td>{!! $player->jersey_number !!}</td>
                <td>{!! $player->country !!}</td>
                <td>
                    {!! Form::open(['route' => ['players.destroy', $player->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('players.show', [$player->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('players.edit', [$player->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
