<!-- Match Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('match_id', 'Match Id:') !!}
    {{ Form::select('match_id', $matches, null, ['class' => 'form-control']) }}
</div>

<!-- Winner Team Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('winner_team_id', 'Winner Team Id:') !!}
    {{ Form::select('winner_team_id', $teams, null, ['class' => 'form-control']) }}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('points.index') !!}" class="btn btn-default">Cancel</a>
</div>
