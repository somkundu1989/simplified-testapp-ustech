<!-- Team Id One Field -->
<div class="form-group col-sm-6">
    {!! Form::label('team_id_one', 'Team One:') !!}
    {{ Form::select('team_id_one', $teams, null, ['class' => 'form-control']) }}
</div>

<!-- Team Id Two Field -->
<div class="form-group col-sm-6">
    {!! Form::label('team_id_two', 'Team Two:') !!}
    {{ Form::select('team_id_two', $teams, null, ['class' => 'form-control']) }}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('matches.index') !!}" class="btn btn-default">Cancel</a>
</div>
