<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $match->id !!}</p>
</div>

<!-- Team Id One Field -->
<div class="form-group">
    {!! Form::label('team_id_one', 'Team One:') !!}
    <p>{!! $match->teamOne->name !!}</p>
</div>

<!-- Team Id Two Field -->
<div class="form-group">
    {!! Form::label('team_id_two', 'Team Two:') !!}
    <p>{!! $match->teamTwo->name !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $match->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $match->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $match->deleted_at !!}</p>
</div>

