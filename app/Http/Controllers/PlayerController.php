<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePlayerRequest;
use App\Http\Requests\UpdatePlayerRequest;
use App\Repositories\PlayerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Repositories\TeamRepository;
use Illuminate\Support\Facades\Storage;

class PlayerController extends AppBaseController
{
    /** @var  PlayerRepository */
    private $playerRepository;
    /** @var  TeamRepository */
    private $teamRepository;

    public function __construct(PlayerRepository $playerRepo, TeamRepository $teamRepo)
    {
        $this->playerRepository = $playerRepo;
        $this->teamRepository = $teamRepo;
    }

    /**
     * Display a listing of the Player.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $players = $this->playerRepository->paginate(10);

        return view('players.index')
            ->with('players', $players);
    }

    /**
     * Show the form for creating a new Player.
     *
     * @return Response
     */
    public function create()
    {
        $teams = $this->teamRepository->pluck('id','name');

        return view('players.create')
            ->with('teams', $teams);
    }

    /**
     * Store a newly created Player in storage.
     *
     * @param CreatePlayerRequest $request
     *
     * @return Response
     */
    public function store(CreatePlayerRequest $request)
    {

        $fileName = "image_".time().'.'.$request->image_uri->getClientOriginalExtension();

        $request->image_uri->storeAs('public/player_images',$fileName);

        $input = $request->all();

        $input['image_uri']=$fileName;

        $player = $this->playerRepository->create($input);

        Flash::success('Player saved successfully.');

        return redirect(route('players.index'));
    }

    /**
     * Display the specified Player.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $player = $this->playerRepository->find($id);

        if (empty($player)) {
            Flash::error('Player not found');

            return redirect(route('players.index'));
        }

        return view('players.show')->with('player', $player);
    }

    /**
     * Show the form for editing the specified Player.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $teams = $this->teamRepository->pluck('id','name');
        $player = $this->playerRepository->find($id);

        if (empty($player)) {
            Flash::error('Player not found');

            return redirect(route('players.index'));
        }

        return view('players.edit')->with('player', $player)->with('teams', $teams);
    }

    /**
     * Update the specified Player in storage.
     *
     * @param int $id
     * @param UpdatePlayerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePlayerRequest $request)
    {
        $player = $this->playerRepository->find($id);

        if (empty($player)) {
            Flash::error('Player not found');

            return redirect(route('players.index'));
        }
        Storage::delete('public/player_images/'.$player->image_uri);

        $fileName = "image_".time().'.'.$request->image_uri->getClientOriginalExtension();

        $request->image_uri->storeAs('public/player_images',$fileName);

        $input = $request->all();

        $input['image_uri']=$fileName;

        $player = $this->playerRepository->update($input, $id);

        Flash::success('Player updated successfully.');

        return redirect(route('players.index'));
    }

    /**
     * Remove the specified Player from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $player = $this->playerRepository->find($id);

        if (empty($player)) {
            Flash::error('Player not found');

            return redirect(route('players.index'));
        }

        $this->playerRepository->delete($id);

        Flash::success('Player deleted successfully.');

        return redirect(route('players.index'));
    }
}
