<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMatchRequest;
use App\Http\Requests\UpdateMatchRequest;
use App\Repositories\MatchRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Repositories\TeamRepository;

class MatchController extends AppBaseController
{
    /** @var  MatchRepository */
    private $matchRepository;
    /** @var  TeamRepository */
    private $teamRepository;

    public function __construct(MatchRepository $matchRepo, TeamRepository $teamRepo)
    {
        $this->matchRepository = $matchRepo;
        $this->teamRepository = $teamRepo;
    }

    /**
     * Display a listing of the Match.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $matches = $this->matchRepository->paginate(10);

        return view('matches.index')
            ->with('matches', $matches);
    }

    /**
     * Show the form for creating a new Match.
     *
     * @return Response
     */
    public function create()
    {
        $teams = $this->teamRepository->pluck('id','name');
        return view('matches.create')
            ->with('teams', $teams);
    }

    /**
     * Store a newly created Match in storage.
     *
     * @param CreateMatchRequest $request
     *
     * @return Response
     */
    public function store(CreateMatchRequest $request)
    {
        $input = $request->all();
        $input['match'] = $request->teamOne->name . ' - vs - ' . $request->teamTwo->name;
        $match = $this->matchRepository->create($input);

        Flash::success('Match saved successfully.');

        return redirect(route('matches.index'));
    }

    /**
     * Display the specified Match.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $match = $this->matchRepository->find($id);

        if (empty($match)) {
            Flash::error('Match not found');

            return redirect(route('matches.index'));
        }

        return view('matches.show')->with('match', $match);
    }

    /**
     * Show the form for editing the specified Match.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $teams = $this->teamRepository->pluck('id','name');
        $match = $this->matchRepository->find($id);

        if (empty($match)) {
            Flash::error('Match not found');

            return redirect(route('matches.index'));
        }

        return view('matches.edit')->with('match', $match)->with('teams', $teams);
    }

    /**
     * Update the specified Match in storage.
     *
     * @param int $id
     * @param UpdateMatchRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMatchRequest $request)
    {
        $match = $this->matchRepository->find($id);

        if (empty($match)) {
            Flash::error('Match not found');

            return redirect(route('matches.index'));
        }

        $input = $request->all();   
        $teamOne = $this->teamRepository->find($request->team_id_one);   
        $teamTwo = $this->teamRepository->find($request->team_id_two);     
        $input['match'] = $teamOne->name . ' - vs - ' . $teamTwo->name;
        $match = $this->matchRepository->update($input, $id);

        Flash::success('Match updated successfully.');

        return redirect(route('matches.index'));
    }

    /**
     * Remove the specified Match from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $match = $this->matchRepository->find($id);

        if (empty($match)) {
            Flash::error('Match not found');

            return redirect(route('matches.index'));
        }

        $this->matchRepository->delete($id);

        Flash::success('Match deleted successfully.');

        return redirect(route('matches.index'));
    }
}
