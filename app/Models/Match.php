<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Match
 * @package App\Models
 * @version July 12, 2019, 10:47 am UTC
 *
 * @property \App\Models\Team teamOne
 * @property \App\Models\Team teamTwo
 * @property \Illuminate\Database\Eloquent\Collection points
 * @property integer team_id_one
 * @property integer team_id_two
 */
class Match extends Model
{
    use SoftDeletes;

    public $table = 'matches';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'match',
        'team_id_one',
        'team_id_two'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'match' => 'string',
        'team_id_one' => 'integer',
        'team_id_two' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'team_id_one' => 'required',
        'team_id_two' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function teamOne()
    {
        return $this->belongsTo(\App\Models\Team::class, 'team_id_one');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function teamTwo()
    {
        return $this->belongsTo(\App\Models\Team::class, 'team_id_two');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function points()
    {
        return $this->hasMany(\App\Models\Point::class);
    }
}
