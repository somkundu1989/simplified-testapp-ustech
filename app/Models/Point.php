<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Point
 * @package App\Models
 * @version July 12, 2019, 10:48 am UTC
 *
 * @property \App\Models\Match match
 * @property \App\Models\Team winnerTeam
 * @property \Illuminate\Database\Eloquent\Collection 
 * @property integer match_id
 * @property integer winner_team_id
 */
class Point extends Model
{
    use SoftDeletes;

    public $table = 'points';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'match_id',
        'winner_team_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'match_id' => 'integer',
        'winner_team_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'match_id' => 'required',
        'winner_team_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function match()
    {
        return $this->belongsTo(\App\Models\Match::class, 'match_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function winnerTeam()
    {
        return $this->belongsTo(\App\Models\Team::class, 'winner_team_id');
    }
}
