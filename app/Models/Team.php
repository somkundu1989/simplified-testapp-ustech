<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Team
 * @package App\Models
 * @version July 12, 2019, 11:02 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection matches
 * @property \Illuminate\Database\Eloquent\Collection matches
 * @property \Illuminate\Database\Eloquent\Collection players
 * @property \Illuminate\Database\Eloquent\Collection points
 * @property string name
 * @property string logo_uri
 * @property string club_state
 */
class Team extends Model
{
    use SoftDeletes;

    public $table = 'teams';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'logo_uri',
        'club_state'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'logo_uri' => 'string',
        'club_state' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'club_state' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function matche_one()
    {
        return $this->hasMany(\App\Models\Match::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function matche_two()
    {
        return $this->hasMany(\App\Models\Match::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function players()
    {
        return $this->hasMany(\App\Models\Player::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function points()
    {
        return $this->hasMany(\App\Models\Point::class);
    }
}
