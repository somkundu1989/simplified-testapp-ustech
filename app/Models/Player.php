<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Player
 * @package App\Models
 * @version July 12, 2019, 11:02 am UTC
 *
 * @property \App\Models\Team team
 * @property \Illuminate\Database\Eloquent\Collection 
 * @property \Illuminate\Database\Eloquent\Collection 
 * @property integer team_id
 * @property string first_name
 * @property string last_name
 * @property string image_uri
 * @property string jersey_number
 * @property string country
 * @property string details
 */
class Player extends Model
{
    use SoftDeletes;

    public $table = 'players';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'team_id',
        'first_name',
        'last_name',
        'image_uri',
        'jersey_number',
        'country',
        'details'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'team_id' => 'integer',
        'first_name' => 'string',
        'last_name' => 'string',
        'image_uri' => 'string',
        'jersey_number' => 'string',
        'country' => 'string',
        'details' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'team_id' => 'required',
        'first_name' => 'required',
        'last_name' => 'required',
        'jersey_number' => 'required',
        'country' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function team()
    {
        return $this->belongsTo(\App\Models\Team::class, 'team_id');
    }
}
