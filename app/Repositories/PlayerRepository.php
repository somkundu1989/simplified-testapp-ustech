<?php

namespace App\Repositories;

use App\Models\Player;
use App\Repositories\BaseRepository;

/**
 * Class PlayerRepository
 * @package App\Repositories
 * @version July 12, 2019, 11:02 am UTC
*/

class PlayerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'team_id',
        'first_name',
        'last_name',
        'image_uri',
        'jersey_number',
        'country',
        'details'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Player::class;
    }
}
