<?php

namespace App\Repositories;

use App\Models\Point;
use App\Repositories\BaseRepository;

/**
 * Class PointRepository
 * @package App\Repositories
 * @version July 12, 2019, 10:48 am UTC
*/

class PointRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'match_id',
        'winner_team_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Point::class;
    }
}
