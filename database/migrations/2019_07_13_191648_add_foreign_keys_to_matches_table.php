<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMatchesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('matches', function(Blueprint $table)
		{
			$table->foreign('team_id_one', 'matches_team_id_one')->references('id')->on('teams')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('team_id_two', 'matches_team_id_two')->references('id')->on('teams')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('matches', function(Blueprint $table)
		{
			$table->dropForeign('matches_team_id_one');
			$table->dropForeign('matches_team_id_two');
		});
	}

}
