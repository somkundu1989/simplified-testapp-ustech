<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPointsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('points', function(Blueprint $table)
		{
			$table->foreign('match_id', 'points_match_id')->references('id')->on('matches')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('winner_team_id', 'points_winner_team_id')->references('id')->on('teams')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('points', function(Blueprint $table)
		{
			$table->dropForeign('points_match_id');
			$table->dropForeign('points_winner_team_id');
		});
	}

}
