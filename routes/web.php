<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/home', 'HomeController@index');
Route::get('/', 'HomeController@index')->name('home');

Auth::routes();
//Route::middleware('auth')->get('/', function (Request $request) {
Route::group(['middleware'=>'auth'], function() {

	Route::resource('teams', 'TeamController');

	Route::resource('players', 'PlayerController');

	Route::resource('matches', 'MatchController');

	Route::resource('points', 'PointController');
});